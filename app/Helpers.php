<?php

use Illuminate\Support\Facades\Auth;

class Helpers
{

    public static function userLevel()
    {
        if (Auth::user()) {
            return Auth::user()->level;
        } else {
            return null;
        }
    }

    public static function listMenu()
    {
        // [
        //     'title' => 'Title menu',
        //     'route' => 'admin.dashboard',
        //     'icon' => '',
        //     'level' => '',
        //     'param' => [],
        //     'sub' => [],

        // ];
        // fas fa-spinner
        return $arrayMenu = [
            [
                'title' => 'Periode Kegiatan',
                'route' => '',
                'icon' => 'fas fa-spinner',
                'level' => 'admin',
                'param' => [
                    'admin.periode.list',
                    'admin.periode.add',
                ],
                'sub' => [
                    [
                        'title' => 'Daftar Periode',
                        'route' => 'admin.periode.list',
                        'icon' => 'fas fa-list',
                        'level' => 'admin',
                        'param' => [

                        ],
                    ],
                    [
                        'title' => 'Tambah Periode',
                        'route' => 'admin.periode.add',
                        'icon' => 'fas fa-plus',
                        'level' => 'admin',
                        'param' => [

                        ],
                    ],
                ],
            ],
            [
                'title' => 'Paket Kegiatan',
                'route' => '',
                'icon' => 'fas fa-spinner',
                'level' => '',
                'param' => [
                    'admin.paket.list',
                    'admin.paket.add',
                ],
                'sub' => [
                    [
                        'title' => 'Daftar Paket',
                        'route' => 'admin.paket.list',
                        'icon' => 'fas fa-list',
                        'level' => 'admin',
                        'param' => [

                        ],
                    ],
                    [
                        'title' => 'Tambah Paket',
                        'route' => 'admin.paket.add',
                        'icon' => 'fas fa-plus',
                        'level' => 'admin',
                        'param' => [

                        ],
                    ],
                ],

            ],
            [
                'title' => 'Pembayaran',
                'route' => '',
                'icon' => 'fas fa-spinner',
                'level' => '',
                'param' => [
                    'admin.metodebayar.list',
                    'admin.metodebayar.add',
                ],
                'sub' => [
                    [
                        'title' => 'Daftar Periode',
                        'route' => 'admin.metodebayar.list',
                        'icon' => 'fas fa-list',
                        'level' => 'admin',
                        'param' => [

                        ],
                    ],
                    [
                        'title' => 'Tambah Periode',
                        'route' => 'admin.metodebayar.add',
                        'icon' => 'fas fa-plus',
                        'level' => 'admin',
                        'param' => [

                        ],
                    ],
                ],

            ],
            [
                'title' => 'Pengguna',
                'route' => '',
                'icon' => 'fas fa-spinner',
                'level' => 'admin',
                'param' => [
                    'admin.user',
                ],
                'sub' => [
                    [
                        'title' => 'Daftar Pengguna',
                        'route' => 'admin.user.list',
                        'icon' => 'fas fa-list',
                        'level' => 'admin',
                        'param' => [

                        ],
                    ],
                    [
                        'title' => 'Tambah Pengguna',
                        'route' => 'admin.user.add',
                        'icon' => 'fas fa-plus',
                        'level' => 'admin',
                        'param' => [

                        ],
                    ],
                ],

            ],

        ];
    }

}
