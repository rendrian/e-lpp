<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    public function __construct()
    {
        if (!Auth::user()) {
            return redirect('admin.dashboard');
        }

    }
    public function index()
    {
        return view('admin.index');
    }

    public function auth(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required|email',
                'password' => 'required',
            ],

        );
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return $this->redirector();
        } else {

            return redirect()->back()->withErrors(['failed' => 'Login Gagal Hubungi Admin'])->withInput();
        }

    }

    public function redirector()
    {
        switch (Auth::user()->level) {
            case 'admin':

                return redirect(route('admin.dashboard'))->with(['success' => 'Wellcome Back']);
                break;
            case 'staff':
                # code...
                break;
            case 'user':
                # code...
                break;

            default:
                # code...
                break;
        }

    }

    public function paket()
    {}
    public function pembayaran()
    {}
    public function user()
    {}
    public function periodeList()
    {

    }

    public function periodeAdd()
    {
        return view('admin.periode.add');

    }

}
