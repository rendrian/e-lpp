<?php

namespace App\Http\Controllers;

use App\Models\BerkasKegiatan;
use Illuminate\Http\Request;

class BerkasKegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BerkasKegiatan  $berkasKegiatan
     * @return \Illuminate\Http\Response
     */
    public function show(BerkasKegiatan $berkasKegiatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BerkasKegiatan  $berkasKegiatan
     * @return \Illuminate\Http\Response
     */
    public function edit(BerkasKegiatan $berkasKegiatan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BerkasKegiatan  $berkasKegiatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BerkasKegiatan $berkasKegiatan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BerkasKegiatan  $berkasKegiatan
     * @return \Illuminate\Http\Response
     */
    public function destroy(BerkasKegiatan $berkasKegiatan)
    {
        //
    }
}
