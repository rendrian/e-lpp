<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AdminPeriode extends Component
{
    public function render()
    {
        return view('livewire.admin-periode');
    }
}
