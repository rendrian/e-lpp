<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class AdminSidebar extends Component
{
    public $menuList;
    public $userLevel;
    public function mount()
    {
        $this->menuList = $this->listMenu();
        $this->userLevel = Auth::user()->level;

    }

    public function buildMenu()
    {

    }

    public function render()
    {
        return view('livewire.admin-sidebar');
    }

}
