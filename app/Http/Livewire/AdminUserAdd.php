<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AdminUserAdd extends Component
{
    public $newUserData;
    public $userLevel;

    // new user data
    public $name;
    public $email;
    public $level = 'peserta';
    public $phone;

    public $password;
    public $password_confirm;

    protected $rules = [
        'name' => "required|min:6",
        'email' => "required|email",
        'phone' => "required|min:10|max:13",
        'password' => "required:min:3|confirmed",
        'password_confirm' => "required:min:3",

    ];

    protected $messages = [
        'name.required' => 'Nama lengkap tidak boleh kosong',
        'name.min' => 'Panjang nama minimal :min',
        'email.required' => 'Email tidak boleh kosong',
        'email.email' => 'Format email salah',
        'phone.required' => 'Nomor HP tidak boleh kosong',
        'phone.min' => 'Panjang nomor HP minimal :min dan maksimal :max',
        'password.required' => 'Kata sandi tidak boleh kosong',
        'password.min' => 'Panjang minimal kata sandi :min karakter',
        'password_confirm.required' => 'Kata sandi tidak boleh kosong',
        'password_confirm.min' => 'Panjang minimal konfirmasi kata sandi :min karakter',

    ];

    public function updated($propertyName)
    {
        switch ($propertyName) {
            case 'email':

                break;

            default:
                # code...
                break;
        }
        $this->validateOnly($propertyName);
    }

    public function mount()
    {
        $this->userLevel = [
            'peserta' => 'Peserta',
            'admin' => 'Admin',
            'staff' => 'Staff',
            'marketing' => 'Admin',
        ];

        /**
         * new user data default value
         */
        $this->newUserData = [
            'name' => "",
            'email' => "",
            'phone' => "",
            'level' => 'peserta',
            'password' => "",
            'password_confirm' => "",

        ];

        $this->formStatus = [
            'button' => null,
        ];

    }

    public function render()
    {
        return view('livewire.admin-user-add');
    }

    /**
     * Fungsi tambah pengguna
     */

    public function addUser()
    {

        $validatedData = $this->validate();

    }

}
