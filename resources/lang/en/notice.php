<?php
/**
 * All Notice
 */
return [
    'empty.password' => 'Password Tidak Boleh Kosong',
    'empty.password_confirm' => 'Konfirmasi Password Tidak Boleh Kosong',
    'wrong.password_confirm' => "Password yang ada masukkan tidak sesuai",
];
