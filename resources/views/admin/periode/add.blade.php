@extends('admin.dashboard')

@section('h1',__('title.h1.admin.periode'))
@section('title',__('title.h1.admin.periode'))
@section('card-header','')

@section('content')
@livewire('admin-periode')
@endsection
