<div>
    <li class="menu-header">{{ $userLevel }} MENU</li>
    @foreach ($menuList as $key=> $item)
    @if (count($item['sub'])>0)

    @else
    <li class="dropdown">
        <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        {{-- <ul class="dropdown-menu">
            <li><a class="nav-link" href="index-0.html">General Dashboard</a></li>
            <li><a class="nav-link" href="index.html">Ecommerce Dashboard</a></li>
        </ul> --}}
    </li>
    <li>
        <a class="nav-link" href="{{ route($item['route']) }}">
            <i class="{{ $item['icon'] }}"></i> <span>{{ $item['title'] }}</span>
        </a>
    </li>
    @endif

    @endforeach

    <li><a class="nav-link" href="blank.html"><i class="far fa-square"></i> <span>Blank Page</span></a></li>
</div>
