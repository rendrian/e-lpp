<div>
    {{-- The whole world belongs to you --}}
    <div class="row">
        <div class="admin-add-user col-lg-6">

            <form wire:submit.prevent="addUser">
                <div class="form-group ">
                    <h2 class="text-primary">@lang('label.admin.user.h2')</h2>
                </div>
                <div class="form-group ">
                    <label for="name">@lang('label.admin.user.name')</label>
                    <input type="text" class="form-control" name="name" id="name" wire:model="name">
                    @error('name') <span class="error text-danger">{{ $message }}</span> @enderror

                </div>
                <div class="form-group">
                    <label for="email">@lang('label.admin.user.email')</label>
                    <input type="text" class="form-control" name="email" id="email" wire:model="email">
                    @error('email') <span class="error text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="phone">@lang('label.admin.user.phone')</label>
                    <input type="text" class="form-control" name="phone" id="phone" wire:model="phone">
                    @error('phone') <span class="error text-danger">{{ $message }}</span> @enderror
                </div>


                <div class="form-group">
                    <label for="level">@lang('label.admin.user.level')</label>
                    <select name="level" id="level" class="form-control" wire:model="level">
                        @foreach ($userLevel as $key=> $item)
                        <option value="{{ $key }}" wire:key="{{ $key }}">{{ $item }}</option>
                        @endforeach

                    </select>
                </div>
                <div class="form-group">
                    <label for="password">@lang('label.admin.user.password')</label>
                    <input type="text" name="password" id="password" class="form-control" wire:model="password">
                    @error('password') <span class="error text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="password">@lang('label.admin.user.password.confirm')</label>
                    <input type="text" name="password_confirm" id="password_confirm" class="form-control"
                        wire:model="password_confirm">
                    @error('password_confirm') <span class="error text-danger">{{ $message }}</span> @enderror
                </div>


                <button class="btn btn-block btn-primary"
                    wire:click="addUser">@lang('label.admin.user.button.submit')</button>

            </form>







        </div>

        <div class="col-lg-6">

        </div>
    </div>
</div>
