<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\MetodePembayaranController;
use App\Http\Controllers\PeriodeKegiatanController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', [AdminController::class, 'index'])->name('admin.dashboard');

    Route::group(['prefix' => 'periode'], function () {
        Route::get('/', [PeriodeKegiatanController::class, 'index'])->name('admin.periode.list');
        Route::get('/add', [PeriodeKegiatanController::class, 'create'])->name('admin.periode.add');

    });

    Route::group(['prefix' => 'paket'], function () {
        Route::get('/', [PeriodeKegiatanController::class, 'index'])->name('admin.paket.list');
        Route::get('/add', [PeriodeKegiatanController::class, 'create'])->name('admin.paket.add');

    });
    Route::group(['prefix' => 'pembayaran'], function () {
        Route::get('/', [MetodePembayaranController::class, 'index'])->name('admin.metodebayar.list');
        Route::get('/add', [MetodePembayaranController::class, 'create'])->name('admin.metodebayar.add');

    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', [UserController::class, 'index'])->name('admin.user.list');
        Route::get('/add', [UserController::class, 'tambahUser'])->name('admin.user.add');
    });

});

Route::post('/auth', [AdminController::class, 'auth'])->name('auth');
Route::any('logout', function () {
    Auth::logout();
    return redirect('/');
})->name('auth.logout');
